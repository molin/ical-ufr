// @ts-check
import globals from "globals";
import tsParser from "@typescript-eslint/parser";
import js from "@eslint/js";
import reactPlugin from "eslint-plugin-react";
import tseslint from "typescript-eslint";
import reactHooks from "eslint-plugin-react-hooks";

export default [
    js.configs.recommended,
    ...tseslint.configs.recommended,
    {
        plugins: {
            react: reactPlugin,
            "react-hooks": reactHooks,
        },

        linterOptions: {
            reportUnusedDisableDirectives: true,
        },

        languageOptions: {
            globals: {
                ...globals.browser,
                ...globals.node,
            },

            ecmaVersion: "latest",
            parser: tsParser,
            sourceType: "module",
        },

        settings: {
            react: {
                version: "detect",
            },
        },

        rules: {
            ...reactHooks.configs.recommended.rules,
            "@typescript-eslint/no-unused-vars": [
                "error", {
                    argsIgnorePattern: "^_",
                },
            ],

            "array-bracket-newline": [
                "warn", {
                    multiline: true,
                },
            ],

            "array-callback-return": "warn",
            "arrow-body-style": ["warn", "as-needed"],
            "comma-dangle": ["error", "always-multiline"],
            curly: "warn",
            eqeqeq: "error",
            "logical-assignment-operators": "error",
            "no-await-in-loop": "warn",
            "no-duplicate-imports": "error",
            "no-param-reassign": "error",
            "no-return-await": "warn",
            "no-template-curly-in-string": "warn",
            "no-unneeded-ternary": "error",
            "no-useless-rename": "warn",
            "no-var": "error",

            "object-curly-newline": [
                "warn", {
                    consistent: true,
                    multiline: true,
                },
            ],

            "object-shorthand": "warn",
            "prefer-const": "warn",
            "prefer-template": "warn",
            quotes: ["error", "double"],
            "require-await": "error",
            semi: ["error", "always"],

            "sort-imports": [
                "warn", {
                    ignoreCase: true,
                    ignoreDeclarationSort: true,
                    ignoreMemberSort: true,
                },
            ],

            "space-before-function-paren": [
                "warn", {
                    anonymous: "always",
                    asyncArrow: "always",
                    named: "never",
                },
            ],

            "spaced-comment": [
                "warn", "always", {
                    markers: ["/"],
                },
            ],
        },
    },
];
