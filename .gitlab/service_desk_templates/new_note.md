Un membre du projet a répondu à votre demande de support technique (numéro de suivi %{ISSUE_ID}) :

%{NOTE_TEXT}

---

Se désabonner : %{UNSUBSCRIBE_URL}
