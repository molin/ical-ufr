Nous avons bien reçu votre demande de support technique.
Elle a reçu le numéro de suivi %{ISSUE_ID}.
Nous vous répondrons dès que possible.

---

Se désabonner : %{UNSUBSCRIBE_URL}
