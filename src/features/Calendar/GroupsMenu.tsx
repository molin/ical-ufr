import React, { useRef, useState } from "react";
import { Source } from "../../groups";
import * as styles from "./GroupsMenu.module.css";

interface GroupsMenuProps {
  allIds: string[];
  /** The possible sources to choose from. */
  sources: Source[];
  /** Whether the sources are currently shown or not. */
  shown: Set<string>;
  /** Callback to set `shown`. */
  setShown: React.Dispatch<React.SetStateAction<Set<string>>>;
  /** Whether to display groups of type annexe or not. */
  showAnnexe: boolean;
  /** Callback to set `showAnnexe`. */
  setShowAnnexe: React.Dispatch<React.SetStateAction<boolean>>;
  /** The current filter. */
  titleFilter: string;
  /** Callback to set `filter`. */
  setTitleFilter: React.Dispatch<React.SetStateAction<string>>;
}

/**
 * A component that allows the user to choose from several groups.
 */
export default function GroupsMenu({ allIds, sources, shown, setShown, showAnnexe, setShowAnnexe, titleFilter, setTitleFilter: setFilter }: GroupsMenuProps) {
  // deal with the "select all" checkbox
  const selectAllRef = useRef<HTMLInputElement>(null);

  const [showDetails, setShowDetails] = useState(false);

  return (
    <div className={styles["menu"]}>
      <div className={styles["list"]}>
        <label className={styles["check"]} htmlFor="checkbox-select-all">
          <input
            id="checkbox-select-all"
            type="checkbox"
            ref={selectAllRef}
            checked={shown.size === allIds.length}
            onChange={(e) => setShown(e.target.checked ? new Set(allIds) : new Set())}
          />
          &nbsp;Tous
        </label>

        {sources.map((source) =>
          <GroupCheckbox
            key={source.id}
            source={source}
            shown={shown} setShown={setShown}
            selectAllRef={selectAllRef}
            total={allIds.length}
            showDetails={showDetails}
          />)}
      </div>

      <details className={styles["options"]}>
        <summary>Options avancées</summary>
        <label>
          <input
            type="checkbox"
            checked={showDetails}
            onChange={(e) => setShowDetails(e.target.checked)}
          />
          <div>Codes des fiches</div>
        </label>

        <label>
          <input
            type="checkbox"
            checked={showAnnexe}
            onChange={(e) => setShowAnnexe(e.target.checked)}
          />
          <div>Fiches annexes</div>
        </label>

        <label>
          <div>Filtre</div>
          <input
            type="text"
            placeholder="Algèbre..."
            value={titleFilter}
            onChange={e => setFilter(e.target.value)}
          />
        </label>
      </details>
    </div>
  );
}

interface GroupCheckboxProps {
  source: Source;
  shown: Set<string>;
  setShown: React.Dispatch<React.SetStateAction<Set<string>>>;
  selectAllRef: React.RefObject<HTMLInputElement | null>;
  total: number;
  showDetails: boolean;
}

/**
 * A checkbox to select a group.
 */
function GroupCheckbox({ source, shown, setShown, selectAllRef, total, showDetails }: GroupCheckboxProps) {
  const details = `${source.parcours.toUpperCase()} ∙ ${source.year.toUpperCase()} ∙ ${source.label} [#${source.id}]`;
  const htmlId = `checkbox-${source.id}`;

  return (
    <label
      htmlFor={htmlId}
      className={styles["check"]}
      data-shown={shown.has(source.id)}
      title={details}
      style={{
        backgroundColor: shown.has(source.id)
          ? source.color
          : "#bbbbbb",
        color: shown.has(source.id) ? source.textColor : "#000000",
      }}
    >
      <input
        id={htmlId}
        type="checkbox"
        checked={shown.has(source.id)}
        onChange={(e) => {
          setShown(curr => {
            if (e.target.checked) {
              curr.add(source.id);
            } else {
              curr.delete(source.id);
            }
            if (selectAllRef.current) {
              // indeterminate if at least one checkbox is checked but not all
              selectAllRef.current.indeterminate = curr.size > 0 && curr.size < total;
            }
            return new Set(curr);
          });
        }} />
      &nbsp;
      {showDetails ? details : source.label}
    </label>
  );
}
